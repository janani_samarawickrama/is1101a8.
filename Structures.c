#include<stdio.h>

struct student
{
  char name[20];
  char subject[20];
  int marks;
};

int main()
{
    int max;
    printf("Enter the number of students:");
    scanf("%d",&max);

    struct student s1[max];
    for(int i=0;i<max;i++)
    {
        printf("Enter name:");
        scanf("%s",s1[i].name);

        printf("Enter subject:");
        scanf("%s",s1[i].subject);

        printf("Enter marks:");
        scanf("%d",&s1[i].marks);
        printf("\n");

        }

        for(int i=0;i<max;i++)
        {
            printf("\n*********************\n");
            printf("Details of the student\n");
            printf("Name of the student:%s\n",s1[i].name);
            printf("Subject:%s\n",s1[i].subject);
            printf("Marks of the student:%d\n",s1[i].marks);
            printf("\n**********************\n");
        }
        return 0;
}
